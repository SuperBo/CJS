FROM python:3

# Update repo and install packages
RUN apt-get update && \
    apt-get -y install nginx postgresql postgresql-contrib git redis-server

# setup environmnet
USER root
ENV djangochat_secret_key="SECRET_KEY" djangochat_db_name="djangochat" \
    djangochat_db_user="djangochatuser" djangochat_db_password="YOUR_PASSWORD"\
    djangochat_postmark_token="POSTMARK_TOKEN" DJANGO_SETTINGS_MODULE=djangochat.settings

# Setup project
RUN git clone --depth=1 https://github.com/ploggingdev/djangochat.git &&\
    cd djangochat && pip install -r requirements.txt

# Setup database
USER postgres
RUN /etc/init.d/postgresql start &&\
    psql --command "CREATE USER djangochatuser WITH PASSWORD 'YOUR_PASSWORD';\
                    ALTER ROLE djangochatuser SET client_encoding TO 'utf8';\
                    ALTER ROLE djangochatuser SET default_transaction_isolation TO 'read committed';\
                    ALTER ROLE djangochatuser SET timezone TO 'UTC';" &&\
    createdb -O djangochatuser djangochat &&\
    cd djangochat && python3 manage.py migrate

# Setup out-port
WORKDIR /djangochat
USER root
EXPOSE 8000
CMD service postgresql start && service redis-server start && python manage.py runserver
